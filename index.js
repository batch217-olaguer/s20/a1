console.log("Hello GG.");

// Number divisible by 5 and 10
// ================================================
let num0 = Number(prompt("Enter a number."));
console.log("The number you provided is " + num0 + ".");

for (let i = num0; i >= 0; i-=5){
	if (i % 10 === 0 && i >= 51){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	} else if(i === 50){
		console.log("The current value is at " + i + ". Terminating the loop.");
		break;
	} else if(i < 50){
		console.log("The current value cannot be lower than 50. Terminating the loop.");
		break;
	}
console.log(i);
}


// Supercalifragilisticexpialidocious Vowel Removal
// =====================================================
let myString = "Supercalifragilisticexpialidocious";
let storeConsonants = "";

for(let i = 0; i < myString.length; i++){


	if(
		myString[i].toLowerCase() == "a" ||
		myString[i].toLowerCase() == "e" ||
		myString[i].toLowerCase() == "i" ||
		myString[i].toLowerCase() == "o" ||
		myString[i].toLowerCase() == "u"
		){
		continue;
	} else {
		storeConsonants += myString[i];
	}
}
console.log(myString);
console.log(storeConsonants);